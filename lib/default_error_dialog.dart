import 'package:dialog_halper/dialog_interface.dart';
import 'package:flutter/material.dart';

class DefaultErrorDialog extends StatelessWidget implements DialogInterface{

 final BuildContext context;

  DefaultErrorDialog(this.context);

  @override
  Widget build(BuildContext context) {
    print( 'default ErrorDialog widget build');
    return AlertDialog(title: Text('\\ awesome dialog /'),);
  }

  @override
 Future show() {
    print( 'default ErrorDialog show');
   return showDialog(context: context, builder: (ctx) => build(ctx)/*AlertDialog(title: Text('\\ awesome dialog /'),)*/);
  }
}
